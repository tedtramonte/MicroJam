uniform sampler2D render;
uniform float Size;
uniform float Dist;

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
vec4 img = v_vColour * texture2D(render,v_vTexcoord);
vec4 yes = v_vColour * texture2D(gm_BaseTexture,v_vTexcoord);

if (distance(vec2(0.5,0.5),v_vTexcoord)<(Dist/Size))
{
   img = yes;
}
   gl_FragColor = img;
}