{
    "id": "2895c635-b62d-439f-8f43-ea9ade3df7cb",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0ab7abcb-0f62-4f71-90b8-a26f8784cafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 185,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 1161,
                "y": 376
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6722af82-71b6-4377-aff6-f2afb8785fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 151,
                "offset": 8,
                "shift": 32,
                "w": 14,
                "x": 1898,
                "y": 376
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "00af4a1f-da0c-4cf0-894e-b627fac6eb6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 87,
                "offset": 8,
                "shift": 56,
                "w": 38,
                "x": 1804,
                "y": 376
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "acdc7bc1-1352-475a-9508-61b808e6c633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 149,
                "offset": 2,
                "shift": 112,
                "w": 110,
                "x": 352,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8811814f-8696-4d52-865c-6576f4740ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 172,
                "offset": 7,
                "shift": 92,
                "w": 73,
                "x": 1394,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "82c0000f-d3e9-45e7-9599-bf944a8e1f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 149,
                "offset": 10,
                "shift": 109,
                "w": 94,
                "x": 945,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ded36d84-f53a-4418-b1c5-baa5588e63e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 154,
                "offset": 5,
                "shift": 87,
                "w": 78,
                "x": 1871,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a1356106-a505-416f-97bb-84dbfba30a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 73,
                "offset": 18,
                "shift": 52,
                "w": 13,
                "x": 1975,
                "y": 376
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b65953c9-a300-4769-b020-84c2da4940ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 175,
                "offset": 7,
                "shift": 49,
                "w": 38,
                "x": 1261,
                "y": 376
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "868db846-23f6-489e-88d9-b325ba06b37a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 175,
                "offset": 7,
                "shift": 49,
                "w": 38,
                "x": 1301,
                "y": 376
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a6dbb11a-a69b-4b96-a412-432407ebb6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 96,
                "offset": 3,
                "shift": 70,
                "w": 59,
                "x": 1557,
                "y": 376
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "04054687-7bdb-4a86-86d7-500f1485ea7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 132,
                "offset": 3,
                "shift": 64,
                "w": 56,
                "x": 1203,
                "y": 376
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "083c6f22-c6b5-4599-bc1e-e141cab47368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 170,
                "offset": 13,
                "shift": 37,
                "w": 20,
                "x": 1782,
                "y": 376
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "36e71252-bdb6-4b1b-8026-99ad26cd5bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 117,
                "offset": 7,
                "shift": 55,
                "w": 42,
                "x": 1659,
                "y": 376
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c562394d-07d0-4d0e-909b-0e98464a6a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 153,
                "offset": 9,
                "shift": 33,
                "w": 16,
                "x": 1863,
                "y": 376
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5cec0362-258f-402b-9c74-16ea491e89f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 153,
                "offset": 5,
                "shift": 68,
                "w": 58,
                "x": 245,
                "y": 376
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "36bf7dea-d42d-4284-8437-42b0d2b68edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 150,
                "offset": 3,
                "shift": 81,
                "w": 73,
                "x": 538,
                "y": 189
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "063bdc0c-5f4e-4002-ac95-5e5da3465f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 147,
                "offset": 10,
                "shift": 60,
                "w": 42,
                "x": 1394,
                "y": 376
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "98e5debe-0e07-47bb-b2e6-c3e2ebcc9b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 147,
                "offset": 11,
                "shift": 81,
                "w": 60,
                "x": 427,
                "y": 376
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "de3933a5-8d67-4952-b3e3-fe5212d8cbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 150,
                "offset": 9,
                "shift": 81,
                "w": 61,
                "x": 1898,
                "y": 189
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2e5854a4-a154-41cc-b24f-93d57964f719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 149,
                "offset": 2,
                "shift": 81,
                "w": 75,
                "x": 385,
                "y": 189
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4923fe5d-f7e9-4a7a-b43b-57250315c75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 151,
                "offset": 8,
                "shift": 81,
                "w": 67,
                "x": 1302,
                "y": 189
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aeb66b4a-996c-4268-9eeb-757483d76819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 152,
                "offset": 7,
                "shift": 81,
                "w": 65,
                "x": 1439,
                "y": 189
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3f67280e-83a3-4701-967a-7adde8b24f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 151,
                "offset": 5,
                "shift": 81,
                "w": 74,
                "x": 462,
                "y": 189
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "91f8d3e6-2e3b-48fb-9627-511840af8fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 151,
                "offset": 6,
                "shift": 81,
                "w": 67,
                "x": 1233,
                "y": 189
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b73248da-cce7-4a28-a180-ce90f8fc3740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 153,
                "offset": 7,
                "shift": 81,
                "w": 69,
                "x": 966,
                "y": 189
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2789f6bf-fbb1-4bc5-9864-56966d9e1f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 139,
                "offset": 12,
                "shift": 40,
                "w": 15,
                "x": 1914,
                "y": 376
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7b30974e-5ac9-4bba-8830-99870417b6ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 159,
                "offset": 5,
                "shift": 40,
                "w": 22,
                "x": 1758,
                "y": 376
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3702a2b0-a0be-492d-b38c-021c2ce26194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 134,
                "offset": 1,
                "shift": 51,
                "w": 39,
                "x": 1618,
                "y": 376
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7d2d8810-9f0f-4fa2-b5e8-39e5a7ff1aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 130,
                "offset": 6,
                "shift": 68,
                "w": 51,
                "x": 1341,
                "y": 376
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a81fd5c2-b57b-4676-a30a-c03b257af7f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 135,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 1511,
                "y": 376
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f7ada51-b443-4de2-8908-81b12a5fbf13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 152,
                "offset": 3,
                "shift": 69,
                "w": 59,
                "x": 64,
                "y": 376
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d423f117-4685-4eb9-955f-1eead93d30c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 156,
                "offset": 7,
                "shift": 124,
                "w": 107,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3c9087d4-5771-4dfd-997b-989cd82b5d8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 149,
                "offset": 9,
                "shift": 97,
                "w": 79,
                "x": 161,
                "y": 189
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "540550f7-3113-4681-92a9-f43a0b404741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 150,
                "offset": 13,
                "shift": 84,
                "w": 66,
                "x": 1371,
                "y": 189
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5d7e6987-3023-43e1-ba54-49a401c74fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 149,
                "offset": 6,
                "shift": 80,
                "w": 72,
                "x": 747,
                "y": 189
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2e8e62cd-6c3b-4ae4-b6b1-55f2b5dc59e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 153,
                "offset": 12,
                "shift": 96,
                "w": 77,
                "x": 2,
                "y": 189
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9f6f466e-0e5d-4a36-8a00-059d91f28ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 153,
                "offset": 9,
                "shift": 83,
                "w": 70,
                "x": 821,
                "y": 189
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d3aabf0f-d62b-4ea1-bd62-ad3bbeea6be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 154,
                "offset": 11,
                "shift": 81,
                "w": 67,
                "x": 1096,
                "y": 189
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "217e7215-b49a-45f4-8cb8-3d9bf5290045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 151,
                "offset": 5,
                "shift": 90,
                "w": 83,
                "x": 1553,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "907bdbca-3943-4835-914c-8f997d99c463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 153,
                "offset": 10,
                "shift": 102,
                "w": 85,
                "x": 1220,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5ceeabf8-bf0f-45b7-aeeb-de271fb12e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 149,
                "offset": 5,
                "shift": 73,
                "w": 64,
                "x": 1703,
                "y": 189
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "03bfbe53-7ed5-478a-9989-bc373a05f95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 155,
                "offset": 6,
                "shift": 88,
                "w": 78,
                "x": 1791,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bda3baf6-d231-4710-8206-1df377896c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 154,
                "offset": 14,
                "shift": 81,
                "w": 66,
                "x": 1165,
                "y": 189
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "17a1fbe9-b4c4-40e8-bf1d-97d703301117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 153,
                "offset": 7,
                "shift": 73,
                "w": 63,
                "x": 1506,
                "y": 189
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "89508f60-68b8-47c0-ba78-1320b26d60d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 152,
                "offset": 7,
                "shift": 117,
                "w": 105,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4cd5ef6a-6446-4bfe-981d-b80fafd522b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 152,
                "offset": 8,
                "shift": 106,
                "w": 93,
                "x": 755,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3082714f-8962-43ad-960f-bf37ede8f782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 151,
                "offset": 8,
                "shift": 106,
                "w": 93,
                "x": 850,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b054776e-5a46-4cda-915f-b607d27e02d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 149,
                "offset": 7,
                "shift": 69,
                "w": 58,
                "x": 608,
                "y": 376
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a49adffe-1ffc-4a6a-ac7e-267ec022b0e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 175,
                "offset": 5,
                "shift": 117,
                "w": 110,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "64d11160-f073-4512-91be-0282eac695e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 150,
                "offset": 8,
                "shift": 84,
                "w": 72,
                "x": 613,
                "y": 189
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "89fa1bff-bc32-45a8-8abe-c2644e60a829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 151,
                "offset": 8,
                "shift": 92,
                "w": 78,
                "x": 81,
                "y": 189
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e672a180-8928-429a-9e9a-91da7714e01f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 148,
                "offset": 7,
                "shift": 90,
                "w": 88,
                "x": 1130,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "95c9b543-2edc-43f0-abb6-2479a62b07ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 150,
                "offset": 9,
                "shift": 98,
                "w": 82,
                "x": 1707,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2999df30-06ab-4327-8929-d7d86dcceb1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 152,
                "offset": 9,
                "shift": 86,
                "w": 77,
                "x": 242,
                "y": 189
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ce5403f1-c1cc-4179-b6d3-91063df356e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 154,
                "offset": 9,
                "shift": 138,
                "w": 127,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3f79e89a-e951-489f-a965-8e0bd2bec459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 152,
                "offset": 4,
                "shift": 96,
                "w": 87,
                "x": 1041,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1ae97d41-78d7-4199-a9b8-8c2c7ad20e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 152,
                "offset": 2,
                "shift": 85,
                "w": 78,
                "x": 1951,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "05e31171-1628-4fa5-892a-b66178c71c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 150,
                "offset": 4,
                "shift": 91,
                "w": 85,
                "x": 1307,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6cefbbcf-fbb7-4663-ae58-94ac43cc9275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 175,
                "offset": 11,
                "shift": 50,
                "w": 35,
                "x": 1438,
                "y": 376
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fb1894cd-fde7-436e-b395-57ae35a70cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 156,
                "offset": 12,
                "shift": 74,
                "w": 55,
                "x": 668,
                "y": 376
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3d8a5d13-d3d9-49b5-a820-9ace25beda33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 175,
                "offset": 12,
                "shift": 50,
                "w": 34,
                "x": 1475,
                "y": 376
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c08e5f99-43bb-45a4-a714-8403293c1763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 74,
                "offset": 13,
                "shift": 77,
                "w": 53,
                "x": 1703,
                "y": 376
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3f19a39f-6474-4692-9a96-5300964a0de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 170,
                "offset": -2,
                "shift": 83,
                "w": 88,
                "x": 571,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d551e614-9386-4b5c-96d3-270d88f236fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 70,
                "offset": 10,
                "shift": 74,
                "w": 28,
                "x": 1945,
                "y": 376
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ae4dffd3-40ad-435f-a6f2-60f37e52327e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 152,
                "offset": 4,
                "shift": 68,
                "w": 62,
                "x": 1834,
                "y": 189
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "96ff6a23-6b9d-4058-87aa-5b42c5bc0e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 150,
                "offset": 10,
                "shift": 79,
                "w": 63,
                "x": 1769,
                "y": 189
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ba0880fb-1c5d-432c-ab4d-a79dde197e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 151,
                "offset": 7,
                "shift": 68,
                "w": 56,
                "x": 783,
                "y": 376
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "05e31320-5d3d-4da2-b1da-71cd25254501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 150,
                "offset": 7,
                "shift": 78,
                "w": 64,
                "x": 1637,
                "y": 189
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "eddae158-a3a6-49a9-b6e4-c8d872c6fd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 150,
                "offset": 6,
                "shift": 73,
                "w": 64,
                "x": 1571,
                "y": 189
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bf2c3d5a-3479-4e6e-9373-c2f22350c6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 157,
                "offset": 5,
                "shift": 68,
                "w": 57,
                "x": 125,
                "y": 376
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8da474a2-27c5-478a-bf6b-7101e61d59a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 184,
                "offset": 4,
                "shift": 71,
                "w": 62,
                "x": 321,
                "y": 189
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9c52f694-af0f-4cab-b586-fbe77a32407c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 150,
                "offset": 9,
                "shift": 77,
                "w": 61,
                "x": 1961,
                "y": 189
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2d5343e9-c9c0-4167-b8fd-007fa6145fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 147,
                "offset": 11,
                "shift": 37,
                "w": 17,
                "x": 1844,
                "y": 376
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0ab5ca7e-8280-4c86-9547-5e146028459a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 186,
                "offset": -1,
                "shift": 54,
                "w": 44,
                "x": 898,
                "y": 376
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e3140571-c8f1-4721-aead-06aede75d0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 150,
                "offset": 11,
                "shift": 72,
                "w": 59,
                "x": 305,
                "y": 376
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6922ef79-2d3b-41e9-8347-5b5d49e61dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 150,
                "offset": 11,
                "shift": 36,
                "w": 15,
                "x": 1881,
                "y": 376
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8f8c178e-ea75-4425-b632-eb56693304c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 154,
                "offset": 7,
                "shift": 103,
                "w": 92,
                "x": 661,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ec502979-e1fa-4a48-aa1b-f2fd63ccdb65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 152,
                "offset": 7,
                "shift": 70,
                "w": 58,
                "x": 489,
                "y": 376
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0b10e795-74f0-4fbb-83cb-06763e3e1af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 151,
                "offset": 5,
                "shift": 70,
                "w": 59,
                "x": 184,
                "y": 376
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "72ac64a5-0cdf-4cc6-a01f-755f28964d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 185,
                "offset": 7,
                "shift": 71,
                "w": 58,
                "x": 687,
                "y": 189
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ff5e944d-dbd4-473a-a61a-b9e390694b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 183,
                "offset": 4,
                "shift": 69,
                "w": 57,
                "x": 1037,
                "y": 189
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0d90ed51-db27-4caf-bbea-ff3d2d30ca30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 151,
                "offset": 9,
                "shift": 64,
                "w": 51,
                "x": 1108,
                "y": 376
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9e7707b9-e127-4b0e-8fd4-cc204620c870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 151,
                "offset": 3,
                "shift": 65,
                "w": 56,
                "x": 725,
                "y": 376
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d5b2c2ac-c43f-4c48-8e78-0d4a5fe78c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 151,
                "offset": 4,
                "shift": 63,
                "w": 55,
                "x": 841,
                "y": 376
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "012d876e-1512-479a-9ff7-8a6cc81035fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 152,
                "offset": 7,
                "shift": 69,
                "w": 57,
                "x": 549,
                "y": 376
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "046514ca-7fad-4f66-94b0-ecc6fad8c6c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 150,
                "offset": 4,
                "shift": 65,
                "w": 59,
                "x": 366,
                "y": 376
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "33467f67-5c2d-43a3-846e-f4aebfc431e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 153,
                "offset": 5,
                "shift": 91,
                "w": 82,
                "x": 1469,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "72440723-9e46-4bde-8e69-450c56b60019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 150,
                "offset": 4,
                "shift": 79,
                "w": 71,
                "x": 893,
                "y": 189
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b15b411d-6849-410a-96eb-0b715697cb19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 185,
                "offset": 0,
                "shift": 69,
                "w": 67,
                "x": 1638,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "78bd768b-134c-4fd6-a086-a0714a7c6a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 152,
                "offset": 8,
                "shift": 72,
                "w": 60,
                "x": 2,
                "y": 376
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fc9dcfd0-ebf3-4fdb-b696-b3577b9acddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 172,
                "offset": 0,
                "shift": 49,
                "w": 45,
                "x": 1061,
                "y": 376
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "933df2a8-fd7c-4949-a931-1025501bb74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 171,
                "offset": 23,
                "shift": 57,
                "w": 12,
                "x": 1931,
                "y": 376
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "88362b9c-4575-4325-9f87-4d6a4d95f067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 172,
                "offset": 0,
                "shift": 49,
                "w": 45,
                "x": 1014,
                "y": 376
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5961ce7f-ffbd-45fe-962c-5b537c81dc7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 117,
                "offset": 7,
                "shift": 80,
                "w": 68,
                "x": 944,
                "y": 376
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 100,
    "styleName": "Regular",
    "textureGroup": 0
}