{
    "id": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMENU",
    "eventList": [
        {
            "id": "a2d97813-ce2b-4b5d-92d2-3ce654954bc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6"
        },
        {
            "id": "d625894b-0be7-4d12-a9a4-0d1ad05819b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6"
        },
        {
            "id": "e1747f1c-623a-4fc8-8b2d-8848794d0ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6"
        },
        {
            "id": "8478ae13-1116-46d4-be73-036777507b26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6"
        },
        {
            "id": "16b651a4-0e48-43a7-883c-2f2a7c0d753e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3ab3f40-6f7e-48e1-8e67-6417948dc0c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}