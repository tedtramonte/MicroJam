/// @description Insert description here
// You can write your code in this editor
ss = surface_create(1280,960)
draw_set_font(font_0)
anim = 800;
rota = 0;
trans= 0;
mode = 0;
phase= 0;

stream = part_system_create()
part_system_depth(stream,1000)

p_type = part_type_create()
part_type_sprite(p_type,spr_part,0,0,1)
part_type_size(p_type,0.01,0.02,0.02,0)
part_type_alpha2(p_type,0.9,1)
part_type_color2(p_type,c_white,c_white)
part_type_orientation(p_type,0,360,8,0,1)
part_type_direction(p_type,0,360,0,0)
part_type_speed(p_type,32,64,0,0)
part_type_life(p_type,20,60)

part_system_automatic_draw(stream,false);
game_set_speed(60,gamespeed_fps)
test = 0