var offset = 10;

up = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up);
down = keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down);
left = keyboard_check_pressed(ord("A")) || keyboard_check_pressed(vk_left);
right = keyboard_check_pressed(ord("D")) || keyboard_check_pressed(vk_right);

if (!down && !left && !right){
	if (audio_is_playing(sndShake) && rest>10){
		if (audio_sound_get_track_position(sndShake)>=.7) {
			audio_stop_sound(sndShake);
			sndShake=OTN_snd_shake;
		}
	}
	rest++;
	posX=room_width/2;
	posY=room_height/2;
}
if(!global.VICTORY && (down || up)){
	rest=0;
	if (!audio_is_playing(sndShake)) sndShake = audio_play_sound(sndShake,1,false);
	posY = (room_height/2)+offset;
	points++;
}
if(!global.VICTORY && left && !right){
	rest=0;
	if (!audio_is_playing(sndShake)) sndShake = audio_play_sound(sndShake,1,false);
	posX = (room_width/2)-offset;
	//posX = lerp(room_width/2,-offset,1);
	points++;
}
if(!global.VICTORY && right && !left){
	rest=0;
	if (!audio_is_playing(sndShake)) sndShake = audio_play_sound(sndShake,1,false);
	posX = (room_width/2)+offset;
	points++;
}

//Hide instructions
if (help==0) instruction=false;
help--;

//Victory
if ((points >= goal) && (sys_obj.timer>sys_obj.max_time)){
	if (!audio_is_playing(OTN_snd_yay) && !global.VICTORY==true) audio_play_sound(OTN_snd_yay,1,false);
	audio_stop_sound(sndShake);
	global.VICTORY=true;
}

if (sys_obj.timer==sys_obj.max_time && !global.VICTORY==true && points<goal){
	if (!audio_is_playing(OTN_snd_boo)) audio_play_sound(OTN_snd_boo,1,false);
	audio_stop_sound(sndShake);
}