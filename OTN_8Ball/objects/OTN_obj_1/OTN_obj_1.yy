{
    "id": "2031b107-8247-4172-a90c-5ff599b6381a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OTN_obj_1",
    "eventList": [
        {
            "id": "215fc6e1-c6bc-44d3-9b13-b0fd8d8847a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2031b107-8247-4172-a90c-5ff599b6381a"
        },
        {
            "id": "5ef09713-3d84-4571-8dc5-0989633804d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2031b107-8247-4172-a90c-5ff599b6381a"
        },
        {
            "id": "84e6ff0c-6457-4238-85ee-0131e7a814cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2031b107-8247-4172-a90c-5ff599b6381a"
        },
        {
            "id": "869b0628-adee-4cb7-9c78-c8ed13d7503f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2031b107-8247-4172-a90c-5ff599b6381a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}