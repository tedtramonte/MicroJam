/// @description Your Drawing goes here
// Draw your stuff! REMEMBER TO REPLACE "user" with your username (Even in UserDefine of sys_obj_1)

if(!sys_obj.timer==0 && global.VICTORY){
	draw_sprite(OTN_s_ball,1,posX,posY);
}
else if (sys_obj.timer==0 && !global.VICTORY){
	draw_sprite(OTN_s_ball,2,posX,posY);
}
else {
	draw_sprite(OTN_s_ball,0,posX,posY);
}

draw_set_halign(fa_middle);
draw_set_valign(fa_middle);
if (instruction) draw_text_transformed(320,240,"Shake!",0.3,0.3,0);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_sprite(spr_bomb,global.frame,0,352);//ticker drawing
