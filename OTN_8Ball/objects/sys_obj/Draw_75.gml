/// @description DONT TOUCH
randomize()
if START==0
{
rota++
rota = rota mod 360;

surface_set_target(SURF)
draw_clear_alpha(0,0)
draw_sprite_ext(spr_wheel,0,320,240,0.5,0.5,rota,-1,1)
part_system_drawit(stream)

if anim==0 {trans-=0.025}
if anim==2 {trans+=0.025}
if trans<=0 && anim==0{anim=1}
if trans>=1 && anim==2{room_goto(objMENU)}

if anim == 1
{
 if ind == 0
 {
 trans_x[ind]+=(32*(SPEED))*(trans_x[ind]<0);
 shake_x[ind]=random(4);
 shake_y[ind]=random(4);
 }
 if ind == 1
 {
 trans_x[ind]-=(32*(SPEED))*(trans_x[ind]>0);
 shake_x[ind]=random(6);
 shake_y[ind]=random(6);
 }
 if ind == 2
 {
 trans_y[ind]+=(32*(SPEED))*(trans_y[ind]<0);
 shake_x[ind]=random(12);
 shake_y[ind]=random(12);
 }
 if ind == 3
 {
 shake_x[ind]=random(4);
 shake_y[ind]=random(4);
 trans_size[ind]-=(0.125*SPEED)*(trans_size[ind]>0);
 trans_rotate[ind]+=10*(SPEED);
 }
 if ind > 3
 {
 shake_x[ind]=random(16);
 shake_y[ind]=random(16);
 trans_size[ind]-=(0.25*SPEED)*(trans_size[ind]>2.5);
 }
 phase+=0.025*(SPEED);
 if floor(phase)<6
 {
 draw_set_halign(fa_center)
 draw_set_valign(fa_center)
 draw_text_transformed(320+trans_x[ind]+shake_x[ind],260+trans_y[ind]+shake_y[ind],
                        txt[ind],trans_size[ind],trans_size[ind],trans_rotate[ind])
 draw_set_halign(fa_left)
 draw_set_valign(fa_top)
 }
 if floor(phase)>=6
 {
  START=1
  instance_create_depth(0,0,0,ENTITY);
 }
 if (ind < 4) ind = floor(phase);
}
surface_reset_target()

draw_surface(SURF,0,0)
draw_set_alpha(trans)
draw_rectangle_color(0,0,800,500,c_white,c_white,c_white,c_white,0)
draw_set_alpha(1)
}
if (START)
{
 shader_set(screen_transition)
 texture_set_stage(shader_get_sampler_index(screen_transition,"render"),surface_get_texture(SURF))
 shader_set_uniform_f(shader_get_uniform(screen_transition,"Dist"),DIST)
 shader_set_uniform_f(shader_get_uniform(screen_transition,"Size"),640)
 global.frame = max_time - ceil(ceil(timer/SPEED)/50);
 if global.frame >= 6 {global.frame = 6}
 draw_surface(application_surface,0,0)//MUST USE DRAW EVENT
 shader_reset()
 if timer > 0 {DIST+=(25*(DIST<800));timer--}
 else
 {
  DIST-=(25*(DIST>0))
  if (DIST<=0)
  {
  event_perform(ev_other,ev_user0);//reset all variables
  instance_destroy_all()
  trans = 0;
  exit
  }
 }
}