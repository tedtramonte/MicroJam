{
    "id": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_obj",
    "eventList": [
        {
            "id": "65bdf9bd-b4a6-420e-b453-b0ff2ea63aca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f"
        },
        {
            "id": "cc78a606-80c5-4be4-995d-5b1de9916399",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f"
        },
        {
            "id": "3e2247cf-f760-4457-94e1-da1229323832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f"
        },
        {
            "id": "21474bd2-f80b-4622-95cb-811a5519e968",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f"
        },
        {
            "id": "8dc346b0-4aa1-481f-94db-74f87a183f06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cd0cdbc-8f85-47bf-a8f6-5178dfeb7b2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}