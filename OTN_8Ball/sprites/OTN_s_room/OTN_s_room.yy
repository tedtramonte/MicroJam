{
    "id": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "OTN_s_room",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "487a23cc-968e-4495-afeb-0f0dce7d709d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "compositeImage": {
                "id": "0387512a-02b3-4236-8a3b-a6753de2c9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82ac3dc-2e2c-456b-aa08-c49c90ebb12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "cec17219-e2dd-4cbb-b661-60e9a840166c"
                },
                {
                    "id": "4221fc44-b3c4-4c03-971b-78449834049d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "68b60e0f-239f-4f1d-8d45-d382c59cbddd"
                },
                {
                    "id": "c578c3e5-4441-473a-b753-0369173a214c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "0db8f82a-881c-4ad0-964e-b079c6125ae5"
                },
                {
                    "id": "b4974997-c73c-4b92-8fed-5a8f49b5c9c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "53f06910-4287-4136-8da4-b65f2c604a26"
                },
                {
                    "id": "0cee425b-41af-4b3f-84eb-a0e2b76b4b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "ade51d53-1166-4627-9be6-7776b377dc51"
                },
                {
                    "id": "c2cd7c9c-7da6-49c3-8f22-569cb889e3b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "6aca3565-4233-4212-b8bc-491c9018421f"
                },
                {
                    "id": "381a5fe4-4ca9-4626-8297-b7869b9882fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487a23cc-968e-4495-afeb-0f0dce7d709d",
                    "LayerId": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6"
                }
            ]
        },
        {
            "id": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "compositeImage": {
                "id": "e20dc503-ad87-4de4-bb12-9fcb650f0e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22489ca1-a4dc-43f9-9f1f-dd397dfb0a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "68b60e0f-239f-4f1d-8d45-d382c59cbddd"
                },
                {
                    "id": "85254094-3e3d-4064-b8c7-9b30cbe4161f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6"
                },
                {
                    "id": "965dec63-cbb1-40d4-99ed-7f79054b1735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "6aca3565-4233-4212-b8bc-491c9018421f"
                },
                {
                    "id": "ee507ca1-14fe-422d-bcae-fa0f6009d778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "ade51d53-1166-4627-9be6-7776b377dc51"
                },
                {
                    "id": "55559f53-07a3-4597-b8e7-7f9fbe1e21b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "53f06910-4287-4136-8da4-b65f2c604a26"
                },
                {
                    "id": "ec40abf1-930c-4d32-90d1-ed6b59e99d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "0db8f82a-881c-4ad0-964e-b079c6125ae5"
                },
                {
                    "id": "e33bcf3f-fb33-4bee-a7c7-11951ec1e092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a1ccc5-2bde-4a70-bef5-756105c31af2",
                    "LayerId": "cec17219-e2dd-4cbb-b661-60e9a840166c"
                }
            ]
        },
        {
            "id": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "compositeImage": {
                "id": "ab068255-16b7-4c29-b1bc-282eb71188e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87c4966-c0e8-4be2-b750-b7dbf3c2a2f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "68b60e0f-239f-4f1d-8d45-d382c59cbddd"
                },
                {
                    "id": "2a01dfb7-616b-4bf9-9121-98e4c7aac72c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6"
                },
                {
                    "id": "ddb096be-4680-4adb-8717-2c69f7c2f9c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "6aca3565-4233-4212-b8bc-491c9018421f"
                },
                {
                    "id": "0249a204-08ad-4734-b5d3-253f178762c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "ade51d53-1166-4627-9be6-7776b377dc51"
                },
                {
                    "id": "4255c7d8-3563-47ec-ad59-4cc571f0628e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "53f06910-4287-4136-8da4-b65f2c604a26"
                },
                {
                    "id": "69cc47bb-8ee2-4b37-917e-a9ca64071355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "0db8f82a-881c-4ad0-964e-b079c6125ae5"
                },
                {
                    "id": "8b2e1e18-a07c-4257-8a1f-92cb9609f872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1873ae-7d69-4c0f-a71c-c535928efc83",
                    "LayerId": "cec17219-e2dd-4cbb-b661-60e9a840166c"
                }
            ]
        },
        {
            "id": "097dca5c-dda4-4cee-949f-588a5f108e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "compositeImage": {
                "id": "f70c9b87-fc61-41d6-aafc-c8c398bb9100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d00c46b-7e6e-4fc9-8436-d56c74423d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "68b60e0f-239f-4f1d-8d45-d382c59cbddd"
                },
                {
                    "id": "b50ab281-339e-4a96-9ec4-e6322df8d834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6"
                },
                {
                    "id": "637ce8ba-e2df-4692-9d93-fd1d2ef1d907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "6aca3565-4233-4212-b8bc-491c9018421f"
                },
                {
                    "id": "014f9da6-f0bd-4901-b0de-6c6f5d55b02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "ade51d53-1166-4627-9be6-7776b377dc51"
                },
                {
                    "id": "e3cb6563-1aad-4e1e-b622-194300146105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "53f06910-4287-4136-8da4-b65f2c604a26"
                },
                {
                    "id": "eeabd2c2-e6a0-4a73-8cc3-f46da3f709f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "0db8f82a-881c-4ad0-964e-b079c6125ae5"
                },
                {
                    "id": "c5c9a969-551a-46bd-bdd6-8d0d92a7df70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097dca5c-dda4-4cee-949f-588a5f108e56",
                    "LayerId": "cec17219-e2dd-4cbb-b661-60e9a840166c"
                }
            ]
        },
        {
            "id": "b7e9dde1-575f-41a8-871b-ca73634783a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "compositeImage": {
                "id": "cb3b3643-1d22-4866-84c3-d0dccc2bb7fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896bae02-6660-4a54-8837-9189bd72626b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "68b60e0f-239f-4f1d-8d45-d382c59cbddd"
                },
                {
                    "id": "981f6446-4d2f-4f14-88cf-5b2973211cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6"
                },
                {
                    "id": "58828d02-b4e8-409a-bae4-b81ef59c9e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "6aca3565-4233-4212-b8bc-491c9018421f"
                },
                {
                    "id": "62cfa5a2-81fa-4e89-b946-14483f33ff20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "ade51d53-1166-4627-9be6-7776b377dc51"
                },
                {
                    "id": "e89c201e-d87d-441e-8674-4a69c8814a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "53f06910-4287-4136-8da4-b65f2c604a26"
                },
                {
                    "id": "5f003aef-e9de-46c0-a20d-8cf62e56e921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "0db8f82a-881c-4ad0-964e-b079c6125ae5"
                },
                {
                    "id": "ae90bf9d-4f80-4328-9606-169a93a5d6b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9dde1-575f-41a8-871b-ca73634783a5",
                    "LayerId": "cec17219-e2dd-4cbb-b661-60e9a840166c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "68b60e0f-239f-4f1d-8d45-d382c59cbddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 30,
            "visible": true
        },
        {
            "id": "6dba9ee9-1925-4ca6-a7b5-f49c9b0a32c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6aca3565-4233-4212-b8bc-491c9018421f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ade51d53-1166-4627-9be6-7776b377dc51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "53f06910-4287-4136-8da4-b65f2c604a26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0db8f82a-881c-4ad0-964e-b079c6125ae5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "cec17219-e2dd-4cbb-b661-60e9a840166c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fb9a35-bf5a-494a-bd83-1388895ce0e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}