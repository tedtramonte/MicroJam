{
    "id": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "57cabf35-9919-47f2-bf42-ee060497b4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "compositeImage": {
                "id": "602eddce-4c57-4cfc-ae7d-6737cd4bcff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57cabf35-9919-47f2-bf42-ee060497b4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36e73079-2746-432f-8255-0b6238c5211a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57cabf35-9919-47f2-bf42-ee060497b4d3",
                    "LayerId": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd"
                }
            ]
        },
        {
            "id": "16803025-fa76-4b85-b62d-44dca034e670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "compositeImage": {
                "id": "cb0f3d0d-bcb6-4d41-937a-3c4867d3fc8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16803025-fa76-4b85-b62d-44dca034e670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3194ab7a-3a1f-475c-9f1c-f89cc82bd590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16803025-fa76-4b85-b62d-44dca034e670",
                    "LayerId": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd"
                }
            ]
        },
        {
            "id": "3dcf665b-ab30-4d9d-ab8b-36bef6a03ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "compositeImage": {
                "id": "808452cd-cb11-49aa-9ae0-a119fef1e81c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dcf665b-ab30-4d9d-ab8b-36bef6a03ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "431f4f15-bf47-493b-8f03-6b252bf71286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dcf665b-ab30-4d9d-ab8b-36bef6a03ce6",
                    "LayerId": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd"
                }
            ]
        },
        {
            "id": "49a12d94-8017-4699-b025-a78417bb63c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "compositeImage": {
                "id": "4ec37a15-7a4f-481a-b594-8102e0c52fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a12d94-8017-4699-b025-a78417bb63c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e7481a-44c2-4d09-b00e-82c2b47df3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a12d94-8017-4699-b025-a78417bb63c2",
                    "LayerId": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd"
                }
            ]
        },
        {
            "id": "7950b6b0-3aa6-454b-9f46-8f90e04fd46b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "compositeImage": {
                "id": "db85ad82-89a7-452e-8698-e2d87f86fc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7950b6b0-3aa6-454b-9f46-8f90e04fd46b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371c9338-2a8f-4211-a36a-1a7575602653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7950b6b0-3aa6-454b-9f46-8f90e04fd46b",
                    "LayerId": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d93da3d2-d270-48b9-9cb8-755ac8f3c6cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a44463d-3148-48b5-a638-49a3f0f1a50f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}