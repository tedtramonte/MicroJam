{
    "id": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "OTN_s_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "daa036a7-d686-4703-a4fe-9585cf59de7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "compositeImage": {
                "id": "44c2a4c1-cb5b-49e5-9f44-19612e703608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa036a7-d686-4703-a4fe-9585cf59de7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f1db29-f26a-49a5-8e42-b0bd3568018d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa036a7-d686-4703-a4fe-9585cf59de7d",
                    "LayerId": "405a42f2-7e1a-4507-8b4c-bcee3daef42d"
                },
                {
                    "id": "7afdca25-ee3c-4b2b-9123-eb58e5401358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa036a7-d686-4703-a4fe-9585cf59de7d",
                    "LayerId": "93d8daf6-c6f6-4659-b8bb-6191ba4577bd"
                },
                {
                    "id": "48671d56-a873-49fe-ace2-1418c25d61b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa036a7-d686-4703-a4fe-9585cf59de7d",
                    "LayerId": "c59c17e1-19a6-4e9c-9df7-b07f25581b43"
                },
                {
                    "id": "678adf7a-383d-4c36-85b2-7afc471c34b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa036a7-d686-4703-a4fe-9585cf59de7d",
                    "LayerId": "171efaad-cba0-48df-9484-ffa279f1dbe9"
                }
            ]
        },
        {
            "id": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "compositeImage": {
                "id": "e633903a-98a0-4de1-8b1a-41dfaf73ce5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bff8141-f439-44af-9b64-6649474f9a86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
                    "LayerId": "405a42f2-7e1a-4507-8b4c-bcee3daef42d"
                },
                {
                    "id": "87127289-dd21-477c-95f9-c3425dfccdd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
                    "LayerId": "93d8daf6-c6f6-4659-b8bb-6191ba4577bd"
                },
                {
                    "id": "100c418a-1bbc-42e8-9693-adcb4ffdb86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
                    "LayerId": "c59c17e1-19a6-4e9c-9df7-b07f25581b43"
                },
                {
                    "id": "3658d18b-d86a-4608-92db-103775040447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c8eef6-5d95-4cff-afba-b419a47afb8f",
                    "LayerId": "171efaad-cba0-48df-9484-ffa279f1dbe9"
                }
            ]
        },
        {
            "id": "db813bf7-302d-42b7-94df-8d533c57999f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "compositeImage": {
                "id": "bf2be3f0-e9e7-4df6-976e-6e593eb1157f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db813bf7-302d-42b7-94df-8d533c57999f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc2b984a-3d39-4123-a125-4311bee500db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db813bf7-302d-42b7-94df-8d533c57999f",
                    "LayerId": "171efaad-cba0-48df-9484-ffa279f1dbe9"
                },
                {
                    "id": "8ba6aa4a-db59-4d58-9544-1c6bfdb74fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db813bf7-302d-42b7-94df-8d533c57999f",
                    "LayerId": "c59c17e1-19a6-4e9c-9df7-b07f25581b43"
                },
                {
                    "id": "b14c4efb-8759-4a3d-addd-59386b7dc0f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db813bf7-302d-42b7-94df-8d533c57999f",
                    "LayerId": "405a42f2-7e1a-4507-8b4c-bcee3daef42d"
                },
                {
                    "id": "5e1c2b48-dc54-4dc2-b18c-ca65df984497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db813bf7-302d-42b7-94df-8d533c57999f",
                    "LayerId": "93d8daf6-c6f6-4659-b8bb-6191ba4577bd"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 480,
    "layers": [
        {
            "id": "171efaad-cba0-48df-9484-ffa279f1dbe9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c59c17e1-19a6-4e9c-9df7-b07f25581b43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "405a42f2-7e1a-4507-8b4c-bcee3daef42d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "93d8daf6-c6f6-4659-b8bb-6191ba4577bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ce32961-4098-4af4-8eb2-c424bb2273d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 240
}