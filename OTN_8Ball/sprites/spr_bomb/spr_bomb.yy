{
    "id": "a435745f-8781-46af-8cb1-f46cd1c123d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7fa02424-b7d7-4b96-b95e-09e0c5887a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "71cf3b95-06d9-447b-8953-0364f0e4cb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa02424-b7d7-4b96-b95e-09e0c5887a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c178f541-7d07-4696-abd7-4f4b7d2157e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa02424-b7d7-4b96-b95e-09e0c5887a9f",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "0591f0de-6cde-43f8-a05b-d12a8b952cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "e202530a-abd3-484e-816f-e70e4f05001a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0591f0de-6cde-43f8-a05b-d12a8b952cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065a90c9-027a-4e60-b074-fc1294436be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0591f0de-6cde-43f8-a05b-d12a8b952cda",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "74064119-c231-449d-ad75-953e67b20679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "d61aa313-8230-492c-abf2-e21da1c11cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74064119-c231-449d-ad75-953e67b20679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f518c8f-ca44-4713-9c04-046a1ee041ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74064119-c231-449d-ad75-953e67b20679",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "24240381-cbe9-403a-b1dc-5ecc67d928dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "f2184a11-fe49-4e21-b537-12b8554bcf89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24240381-cbe9-403a-b1dc-5ecc67d928dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1018267a-fc6f-49fd-b4e9-09a1f7ad4526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24240381-cbe9-403a-b1dc-5ecc67d928dc",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "e1732a15-2020-4635-96c2-043085aab5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "294d2f22-6dde-4131-a52f-be9f47f976a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1732a15-2020-4635-96c2-043085aab5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b1a012-52cc-4d0b-a589-d1c7a3e254a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1732a15-2020-4635-96c2-043085aab5e0",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "9020a5a1-149c-430f-a854-1e590381a000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "8b5e93c0-7d06-4732-a82e-1982e80aa766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9020a5a1-149c-430f-a854-1e590381a000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "829251dd-a19c-4931-b530-d3b547e5434b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9020a5a1-149c-430f-a854-1e590381a000",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        },
        {
            "id": "37d085cd-9e6b-4a35-8440-e000283dbe29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "compositeImage": {
                "id": "f808747b-ea9a-42a1-aa50-ae580e571b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d085cd-9e6b-4a35-8440-e000283dbe29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598506ac-d8b5-47f7-9574-9d8ee95fbc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d085cd-9e6b-4a35-8440-e000283dbe29",
                    "LayerId": "4e97a44e-878c-4cb7-b117-c5611cf0ca27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4e97a44e-878c-4cb7-b117-c5611cf0ca27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a435745f-8781-46af-8cb1-f46cd1c123d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}