{
    "id": "9678362b-3571-4a6f-99f4-3bc9a356b6c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wheel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1999,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b4c826e0-c791-461f-a68c-a393c173ba89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9678362b-3571-4a6f-99f4-3bc9a356b6c4",
            "compositeImage": {
                "id": "bbcdae33-dcf1-46e1-ae76-b7e7f230bb60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c826e0-c791-461f-a68c-a393c173ba89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5601acf-2906-469e-aea9-ae8ce2d7c39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c826e0-c791-461f-a68c-a393c173ba89",
                    "LayerId": "60f27e3a-13ee-4b7e-bff0-3f4e45705781"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "60f27e3a-13ee-4b7e-bff0-3f4e45705781",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9678362b-3571-4a6f-99f4-3bc9a356b6c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 2000,
    "xorig": 998,
    "yorig": 1003
}