{
    "id": "4d7c1271-a7c8-41b1-a410-4dee0609f9f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 371,
    "bbox_left": 0,
    "bbox_right": 950,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "09e97cb9-6354-48ed-9c17-a3da3e8f5986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d7c1271-a7c8-41b1-a410-4dee0609f9f8",
            "compositeImage": {
                "id": "6c7cbfde-92e1-4529-93ca-fed5d1f8f25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e97cb9-6354-48ed-9c17-a3da3e8f5986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b707078-389b-4e3a-aa47-d45444feffd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e97cb9-6354-48ed-9c17-a3da3e8f5986",
                    "LayerId": "c2eab7ce-dfc4-466a-b0d3-03f8eba5e840"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 372,
    "layers": [
        {
            "id": "c2eab7ce-dfc4-466a-b0d3-03f8eba5e840",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d7c1271-a7c8-41b1-a410-4dee0609f9f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 951,
    "xorig": 475,
    "yorig": 186
}