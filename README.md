Shake! is my entry to a community-member ran Game Jam themed around creating a microgame in the style of Warioware, and my first ever "released" game. Shake! won third place, a dubious honor given the context (the forum thread should make this clear).

Game jam forum thread: https://forum.yoyogames.com/index.php?threads/the-micro-jam-theme-crazy-party.25381/

All programming, art, and sounds were created by me from scratch over the weekend of the jam. I think the gameplay, style, and sound effects capture the essence of a Warioware microgame very well, and I'm oddly proud of this little project.